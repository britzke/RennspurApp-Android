package de.lette.rennspur;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class PositionManager {

    private JSONArray positions;
    private String hash;


    public PositionManager(String hash) {
        positions = new JSONArray();
        this.hash = hash;
    }

    public void clear() {
        positions = new JSONArray();
    }

    public void add(Position position) {
        positions.put(position.toJson());
    }

    public JSONObject toJson() {
        try {
            return new JSONObject().put("hash", hash).put("positions", positions);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }

    }
}

package de.lette.rennspur;

import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

public class Position {
    private double la;
    private double lo;
    private long date;

    public Position(Location location) {
        la = location.getLatitude();
        lo = location.getLongitude();
        date = System.currentTimeMillis();
    }

    public JSONObject toJson() {
        try {
            return new JSONObject()
                    .put("time", date)
                    .put("latitude", la)
                    .put("longitude", lo);
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }
}
